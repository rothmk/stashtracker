package edu.rosehulman.stashtracker;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import android.util.Log;

import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;

public class OAuthHelper {

	private OAuthConsumer mConsumer;
	private OAuthProvider mProvider;

	private String mCallbackUrl;

	public OAuthHelper(String consumerKey, String consumerSecret, String scope,
			String callbackUrl) throws UnsupportedEncodingException {
		mConsumer = new CommonsHttpOAuthConsumer(consumerKey, consumerSecret);
		Log.d(MainActivity.ST, "Keys: " + consumerKey + ", " + consumerSecret);
		mProvider = new CommonsHttpOAuthProvider(
				"https://api.ravelry.com/oauth/request_token",
				"https://api.ravelry.com/oauth/access_token",
				"https://api.ravelry.com/oauth/authorize");
		mProvider.setOAuth10a(true);
		mCallbackUrl = (callbackUrl == null ? OAuth.OUT_OF_BAND : callbackUrl);
	}

	public String getRequestToken() throws OAuthMessageSignerException,
			OAuthNotAuthorizedException, OAuthExpectationFailedException,
			OAuthCommunicationException {
		String authUrl = mProvider
				.retrieveRequestToken(mConsumer, mCallbackUrl);
		return authUrl;
	}
	
	public String[] getAccessToken(String verifier)
			throws OAuthMessageSignerException, OAuthNotAuthorizedException,
			OAuthExpectationFailedException, OAuthCommunicationException {
				mProvider.retrieveAccessToken(mConsumer, verifier);
				return new String[] {
						mConsumer.getToken(), mConsumer.getTokenSecret()
				};
			}


}
