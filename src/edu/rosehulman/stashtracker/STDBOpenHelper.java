package edu.rosehulman.stashtracker;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class STDBOpenHelper extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "StashTrackerDB.db";
	public static final String DATABASE_TABLE = "YarnInfo";
	public static final int DATABASE_VERSION = 1;

	private static final String DATABASE_CREATE = "create table "
			+ DATABASE_TABLE + " (" + STContentProvider.KEY_ID
			+ " integer primary key autoincrement, "
			+ STContentProvider.KEY_YARN_NAME_COLUMN + " text not null, "
			+ STContentProvider.KEY_YARN_WEIGHT_COLUMN + " int, "
			+ STContentProvider.KEY_YARN_AMT_STASHED_COLUMN + " integer);";

	public STDBOpenHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public STDBOpenHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w("TaskDBAdapter", "Upgrading from version " + oldVersion + " to "
				+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF IT EXISTS " + DATABASE_TABLE);
		onCreate(db);

	}

//	@Override
//	public static SQLiteDatabase getWritableDatabase() {
//		// TODO Auto-generated method stub
//		return super.getWritableDatabase();
//	}

}
