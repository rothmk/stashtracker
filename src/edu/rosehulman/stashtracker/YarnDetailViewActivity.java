package edu.rosehulman.stashtracker;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class YarnDetailViewActivity extends Activity {

	String yarnName;
	Yarn yarn = new Yarn();
	TextView name;
	TextView weight;
	TextView amtStashed;
	TextView purchasedAt;
	TextView purchaseDate;
	TextView price;
	TextView dyeLot;
	TextView colorway;
	TextView colorFamily;
	TextView tags;
	TextView notes;

	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_yarn_detail_view);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		Intent data = this.getIntent();
		yarnName = data.getStringExtra(MainActivity.KEY_YARN_NAME);
		yarn = MainActivity.mDbSTAdapter.getYarn(yarnName);

		// dummy data
		// yarn.setName("dino");
		// yarn.setWeight(5);
		// yarn.setAmtStashed("3");
		// yarn.setPurchasedAt("JoAnn's");
		// yarn.setPurchaseDate(2012, 6, 23);
		// yarn.setPricePaid("3.12");
		// yarn.setDyeLot("423");
		// yarn.setColorWay("Grass Green");
		// yarn.setColorFamily(7);
		// yarn.setTags("Peaches & Creme; amigurumi");
		// yarn.setNotes("using this to make the dino body");

		name = (TextView) findViewById(R.id.detail_yarn_name);
		weight = (TextView) findViewById(R.id.detail_yarn_weight);
		amtStashed = (TextView) findViewById(R.id.detail_amount_stashed);
		purchasedAt = (TextView) findViewById(R.id.detail_purchased_at);
		purchaseDate = (TextView) findViewById(R.id.detail_purchase_date);
		price = (TextView) findViewById(R.id.detail_price);
		dyeLot = (TextView) findViewById(R.id.detail_dye_lot);
		colorway = (TextView) findViewById(R.id.detail_colorway);
		colorFamily = (TextView) findViewById(R.id.detail_color_family);
		tags = (TextView) findViewById(R.id.detail_tags);
		notes = (TextView) findViewById(R.id.detail_notes);

		Log.d(MainActivity.ST, yarn.toString());
		name.setText(yarn.getName());
		weight.setText(yarn.getWeightAsString());
		amtStashed.setText(yarn.getAmtStashed());
		purchasedAt.setText(yarn.getPurchasedAt());
		purchaseDate.setText(yarn.getPurchaseDateAsString());
		price.setText(getString(R.string.dolla).toString()
				+ yarn.getPricePaid());
		dyeLot.setText(yarn.getDyeLot());
		colorway.setText(yarn.getColorWay());
		colorFamily.setText(yarn.getColorFamilyAsString());
		tags.setText(yarn.getTags());
		notes.setText(yarn.getNotes());

		Button editButton = (Button) findViewById(R.id.button_edit);
		editButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent editYarnIntent = new Intent(YarnDetailViewActivity.this,
						EditYarnActivity.class);
				editYarnIntent.putExtra(MainActivity.KEY_NEW_YARN, false);
				editYarnIntent.putExtra(MainActivity.KEY_YARN_NAME,
						yarn.getName());
				YarnDetailViewActivity.this.startActivity(editYarnIntent);
			}
		});

		Button deleteButton = (Button) findViewById(R.id.button_delete);
		deleteButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				MainActivity.removeYarn(yarn);
				Intent viewYarnIntent = new Intent(YarnDetailViewActivity.this,
						YarnViewActivity.class);
				YarnDetailViewActivity.this.startActivity(viewYarnIntent);
			}
		});

		/*
		 * 
		 * 
		 * 
		 * <ListView android:id="@+id/yarn_listview"
		 * android:layout_width="match_parent"
		 * android:layout_height="wrap_content"
		 * android:layout_centerHorizontal="true"
		 * android:layout_centerVertical="true" > </ListView>
		 * 
		 * 
		 * 
		 * ListView listView = (ListView) findViewById(R.id.yarn_listview);
		 * String[] values = new String[] { "Yarn 1", "Yarn 2", "Yarn 3",
		 * "Yarn 4" };
		 * 
		 * // First paramenter - Context // Second parameter - Layout for the
		 * row // Third parameter - ID of the TextView to which the data is
		 * written // Forth - the Array of data ArrayAdapter<String> adapter =
		 * new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
		 * android.R.id.text1, values);
		 * 
		 * // Assign adapter to ListView listView.setAdapter(adapter);
		 * 
		 * listView.setOnItemClickListener(new OnItemClickListener() {
		 * 
		 * @Override public void onItemClick(AdapterView<?> parent, View view,
		 * int position, long id) { Intent detailIntent = new
		 * Intent(YarnViewActivity.this, YarnDetailViewActivity.class);
		 * 
		 * } });
		 */
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_yarn_detail_view, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent homeIntent = new Intent(YarnDetailViewActivity.this,
					MainActivity.class);
			YarnDetailViewActivity.this.startActivity(homeIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
