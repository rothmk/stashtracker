package edu.rosehulman.stashtracker;

import java.io.UnsupportedEncodingException;
import java.util.Collection;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class OAuthActivity extends Activity {

	String consumerKey = "7BC2DA8E3D97A71DFCC4";
	String consumerSecret = "ABXntUjz47yvLe+/h6q7aMGNicWhBoLeId64EJkt";
	String currentUser = "";
	OAuthHelper helper;

	private String CALLBACKURL = "app://ravelry";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_oauth);
		StrictMode.enableDefaults();
		try {
			helper = new OAuthHelper(consumerKey, consumerSecret, "", CALLBACKURL);
		} catch (UnsupportedEncodingException e1) {
			Log.d(MainActivity.ST, "Helper wasn't assigned, btw");
			e1.printStackTrace();
		}

		Button sync = (Button) findViewById(R.id.sync_button);
		sync.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				Log.d(MainActivity.ST, "'Sync With Ravelry' button clicked");
				Toast.makeText(OAuthActivity.this, R.string.now_syncing,
						Toast.LENGTH_LONG).show();
				
				

			}
		});

		Button oauth = (Button) findViewById(R.id.oauth_button);
		oauth.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				Log.d(MainActivity.ST, "Login button clicked");
				
				try {
					String uri = helper.getRequestToken();
					
					Log.d(MainActivity.ST, "uri: " + uri);
					startActivity(new Intent("android.intent.action.VIEW", Uri.parse(uri)));
				} catch (Exception e) {
					Log.d(MainActivity.ST, "Exception in the hizzouse");
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	protected void onResume() {
		String[] token = getVerifier();
		if (token != null) {
			try {
				helper.getAccessToken(token[1]);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		super.onResume();
	}

	private String[] getVerifier() {
		// extract the token if it exists
		Uri uri = this.getIntent().getData();
		if (uri == null) {
			return null;
		}

		String token = uri.getQueryParameter("oauth_token");
		String verifier = uri.getQueryParameter("oauth_verifier");
		return new String[] { token, verifier };
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_oauth, menu);
		return true;
	}

	public class RavSyncerTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			String[] accessToken = null;
			String[] token = getVerifier();
			String url = "https://api.ravelry.com/people/osaat/stash/list.json";

			if (token != null) {
				try {
					accessToken = helper.getAccessToken(token[1]);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			if(accessToken == null){
				Log.d(MainActivity.ST, "accessToken null.");
			}

			OAuthConsumer consumer = new CommonsHttpOAuthConsumer(accessToken[0], accessToken[1]);

			HttpGet request = new HttpGet(url);

			// sign the request
			try {
				consumer.sign(request);

				// send the request
				HttpClient httpClient = new DefaultHttpClient();
				HttpResponse response = httpClient.execute(request);
				JSONArray array = new JSONArray((Collection) response);
				
				Log.d(MainActivity.ST, array.toString());

			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent homeIntent = new Intent(OAuthActivity.this,
					MainActivity.class);
			OAuthActivity.this.startActivity(homeIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
