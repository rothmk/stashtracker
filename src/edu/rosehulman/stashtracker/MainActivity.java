package edu.rosehulman.stashtracker;

import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.ParseException;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends Activity implements OnClickListener {

	public final static String ST = "ST";
	public final static String KEY_NEW_YARN = "KEY_NEW_YARN";
	public final static String KEY_YARN_NAME = "KEY_YARN_NAME";
	public final static String KEY_YARN_ID = "KEY_YARN_ID";

	public static STAdapter mDbSTAdapter;
	public static SimpleCursorAdapter mSTAdapter;
	public static Cursor mSTCursor;

	public static final long NO_ID_SELECTED = -1;
	private static final long mSelectedId = NO_ID_SELECTED;

	String consumerKey = "7BC2DA8E3D97A71DFCC4";
	String consumerSecret = "ABXntUjz47yvLe+/h6q7aMGNicWhBoLeId64EJkt";

	public static Resources res;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		res = getResources();

		((Button) findViewById(R.id.add_yarn_button)).setOnClickListener(this);
		((Button) findViewById(R.id.view_yarn_button)).setOnClickListener(this);
		((Button) findViewById(R.id.sync_button)).setOnClickListener(this);

		mDbSTAdapter = new STAdapter(this);
		mDbSTAdapter.open();

		Cursor cursor = mDbSTAdapter.getYarnCursor();

		String[] fromColumns = new String[] { STAdapter.KEY_YARN_NAME_COLUMN,
				STAdapter.KEY_YARN_WEIGHT_COLUMN,
				STAdapter.KEY_YARN_AMT_STASHED_COLUMN };
		int[] toTextViews = new int[] { R.id.input_yarn_name,
				R.id.input_yarn_weight, R.id.input_amount_stashed };
		mSTAdapter = new SimpleCursorAdapter(this, R.layout.activity_yarn_view,
				cursor, fromColumns, toTextViews, 0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.add_yarn_button:
			Log.d(ST, "'Add Yarn' button clicked");
			Intent newYarnIntent = new Intent(this, EditYarnActivity.class);
			newYarnIntent.putExtra(KEY_NEW_YARN, true);

			this.startActivity(newYarnIntent);
			break;
		case R.id.view_yarn_button:
			Log.d(ST, "'View Yarn' button clicked");
			Intent viewYarnIntent = new Intent(this, YarnViewActivity.class);
			this.startActivity(viewYarnIntent);

			break;

		case R.id.sync_button:

			Intent intent = new Intent(this, OAuthActivity.class);
			this.startActivity(intent);

			// mDbSTAdapter.clearDb();
			// Toast.makeText(this, "Cleared database", Toast.LENGTH_SHORT)
			// .show();

			break;
		}
	}

	public String getContentCharSet(final HttpEntity entity)
			throws ParseException {

		if (entity == null) {
			throw new IllegalArgumentException("HTTP entity may not be null");
		}

		String charset = null;

		if (entity.getContentType() != null) {

			HeaderElement values[] = entity.getContentType().getElements();

			if (values.length > 0) {

				NameValuePair param = values[0].getParameterByName("charset");

				if (param != null) {

					charset = param.getValue();

				}

			}

		}

		return charset;

	}

	public static void addYarn(Yarn yarn) {
		mDbSTAdapter.addYarn(yarn);
		mSTAdapter.changeCursor(mDbSTAdapter.getYarnCursor());
	}

	public static Yarn getYarn(int id) {
		return mDbSTAdapter.getYarn(id);
	}

	public static Yarn getYarn(String name) {
		return mDbSTAdapter.getYarn(name);
	}

	public static void editYarn(Yarn yarn) {
		if (mSelectedId == NO_ID_SELECTED) {
			Log.e(MainActivity.ST, "Attempt to update with no yarn selected.");
		}
		yarn.setID((int) mSelectedId);
		mDbSTAdapter.updateYarn(yarn);
		mSTAdapter.changeCursor(mDbSTAdapter.getYarnCursor());
	}

	public static void removeYarn(int id) {
		mDbSTAdapter.removeYarn(id);
		Cursor cursor = mDbSTAdapter.getYarnCursor();
		mSTAdapter.changeCursor(cursor);
	}

	public static void removeYarn(Yarn yarn) {
		mDbSTAdapter.removeYarn(yarn.getID());
		Cursor cursor = mDbSTAdapter.getYarnCursor();
		mSTAdapter.changeCursor(cursor);
	}

}
