package edu.rosehulman.stashtracker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

public class EditYarnActivity extends Activity implements
		OnItemSelectedListener {
	private boolean mIsNewYarn;
	private String mYarnName;

	private Yarn yarn;
	private ArrayAdapter<CharSequence> colorAdapter;
	private ArrayAdapter<CharSequence> weightAdapter;

	private AutoCompleteTextView name;
	private Spinner weightSpinner;
	private EditText amt;
	private AutoCompleteTextView purchasedAt;
	private DatePicker purchaseDate;
	private EditText price;
	private EditText dyeLot;
	private EditText colorWay;
	private Spinner colorSpinner;
	private EditText tags;
	private EditText notes;

	private HashMap<String, ArrayList<String>> mYarnAssociations;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_yarn);

		Intent data = this.getIntent();
		mIsNewYarn = data.getBooleanExtra(MainActivity.KEY_NEW_YARN, true);
		Log.d(MainActivity.ST, "New/edit screen created, new yarn = "
				+ mIsNewYarn);

		name = (AutoCompleteTextView) findViewById(R.id.input_yarn_name);
		amt = (EditText) findViewById(R.id.input_amount_stashed);
		purchasedAt = (AutoCompleteTextView) findViewById(R.id.input_purchased_at);
		purchaseDate = (DatePicker) findViewById(R.id.input_purchase_date);
		price = (EditText) findViewById(R.id.input_price);
		dyeLot = (EditText) findViewById(R.id.input_dye_lot);
		colorWay = (EditText) findViewById(R.id.input_colorway);
		tags = (EditText) findViewById(R.id.input_tags);
		notes = (EditText) findViewById(R.id.input_notes);
		
		mYarnAssociations = new HashMap<String, ArrayList<String>>();
		initializeHashMap();

		String[] yarnList = getResources().getStringArray(R.array.yarn_list);
		ArrayAdapter<String> yarnListAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, yarnList);
		name.setAdapter(yarnListAdapter);

		String[] storeList = getResources().getStringArray(R.array.store_list);
		ArrayAdapter<String> storeListAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, storeList);
		purchasedAt.setAdapter(storeListAdapter);

		name.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View v, int pos,
					long id) {

				String item = (String) arg0.getItemAtPosition(pos);
				setItemWeight(item);
			}
		});

		weightSpinner = (Spinner) findViewById(R.id.input_yarn_weight);
		weightAdapter = ArrayAdapter.createFromResource(this,
				R.array.yarn_weights, android.R.layout.simple_spinner_item);
		weightAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		weightSpinner.setAdapter(weightAdapter);
		weightSpinner.setOnItemSelectedListener(this);

		colorSpinner = (Spinner) findViewById(R.id.input_color_family);
		colorAdapter = ArrayAdapter.createFromResource(this,
				R.array.closest_color, android.R.layout.simple_spinner_item);
		colorAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		colorSpinner.setAdapter(colorAdapter);
		colorSpinner.setOnItemSelectedListener(this);

		if (!mIsNewYarn) {
			mYarnName = data.getStringExtra(MainActivity.KEY_YARN_NAME);
			yarn = MainActivity.getYarn(mYarnName);
			name.setText(yarn.getName());
			weightSpinner.setSelection(yarn.getWeight());
			amt.setText(yarn.getAmtStashed());
			purchasedAt.setText(yarn.getPurchasedAt());
			purchaseDate.updateDate(
					yarn.getPurchaseDate().get(GregorianCalendar.YEAR),
					yarn.getPurchaseDate().get(GregorianCalendar.MONTH) - 1,
					yarn.getPurchaseDate().get(GregorianCalendar.DAY_OF_MONTH));
			price.setText(yarn.getPricePaid());
			dyeLot.setText(yarn.getDyeLot());
			colorWay.setText(yarn.getColorWay());
			colorSpinner.setSelection(yarn.getColorFamily());
			tags.setText(yarn.getTags());
			notes.setText(yarn.getNotes());
		} else {
			yarn = new Yarn();
			MainActivity.addYarn(yarn);
		}

		((Button) findViewById(R.id.button_ok))
				.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {

						yarn.setName(name.getText().toString());
						yarn.setAmtStashed(amt.getText().toString());
						yarn.setPurchasedAt(purchasedAt.getText().toString());
						yarn.setPurchaseDate(purchaseDate.getYear(),
								purchaseDate.getMonth() + 1,
								purchaseDate.getDayOfMonth());
						yarn.setPricePaid(price.getText().toString());
						yarn.setDyeLot(dyeLot.getText().toString());
						yarn.setColorWay(colorWay.getText().toString());
						yarn.setTags(tags.getText().toString());
						yarn.setNotes(notes.getText().toString());

						if (mIsNewYarn) {
							MainActivity.addYarn(yarn);
						} else {
							MainActivity.editYarn(yarn);
						}

						Log.d(MainActivity.ST, "just added to database: "
								+ yarn.toString());
						finish();
					}
				});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_edit_yarn, menu);
		return true;
	}
	
	

	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		Log.d(MainActivity.ST, "I selected a thing!");
		if (parent.equals(weightSpinner)) {
			yarn.setWeight(position);
			Log.d(MainActivity.ST, "Set color family to: " + yarn.getWeight());
		} else if (parent.equals(colorSpinner)) {
			yarn.setColorFamily(position);
			Log.d(MainActivity.ST,
					"Set color family to: " + yarn.getColorFamily());
		}
	}

	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	private void setItemWeight(String selected) {
		String[] weights = getResources().getStringArray(R.array.yarn_weights);

		for (int i = 0; i < weights.length; i++) {
			if (mYarnAssociations.get(weights[i]).contains(selected)) {
				weightSpinner.setSelection(i);
			}
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent homeIntent = new Intent(EditYarnActivity.this,
					MainActivity.class);
			EditYarnActivity.this.startActivity(homeIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void initializeHashMap() {
		String[] weights = getResources().getStringArray(R.array.yarn_weights);

		if (weights != null) {
			String[] none = { "Accent Worsted", "Alafoss Lopi", "Alaska",
					"Alpaca", "Alpaca Cotton", "Alpakka", "Amara", "Ambiance",
					"Ambiance print", "Ara", "Arequipa" };

			String[] thread = { "" };

			String[] cobweb = { "Aspire Super Chunky" };

			String[] lace = { "Baby 4 Ply", "Baby Big Value DK", "Baby Marble",
					"Baby Twist", "Baby Velvet DK", "Baby Wool 4 Ply", "Bella",
					"Berroco Sox Metallic", "Best of Effects 1", "Big Baby",
					"Big Fabel", "Biosoja", "Bonsai" };

			String[] lightFingering = { "Bottone Tweed", "Bubble",
					"Bubble Print", "Bulky Lopi", "Butterfly Super 10",
					"Buttons D.K.", "Candy D.K.", "Candy Floss", "Carat",
					"Cascade 220 Hand Paints", "Cashsoft Aran", "Catania", "Cento",
					"Centolavaggi", "Chiara" };

			String[] fingering = { "Chili", "Chunky 2-ply", "Classic Silk",
					"Classic Wool", "Cloud Cotton Eco", "Clouds", "Clown",
					"Colourscape Chunky", "Comfort", "Comfort Chunky",
					"Comfort DK", "Comfort Sock", "CoolSpun Cotton",
					"Cotton Weekend Color", "CottonTail", "Cotton-Viscose",
					"Creative Focus Brushed Alpaca",
					"Creative Focus Kid Mohair", "Creative Focus Silk",
					"Cupcake", "Cuzco", "Daylily", "Desert Flower",
					"Design Line", "Design Line Color", "Divine" };

			String[] sport = { "Duett", "Dune", "Duo", "ECO Baby Bomull",
					"ECO Baby Ull", "ECO Baby Ull Color", "ECO Multi Ull",
					"Eco Super Alpaca", "ECO Ull", "ECO Ull Color",
					"Ecological Wool", "Eden", "Eskimo", "Eskimo mix",
					"Eskimo variegated", "Exotic Color" };

			String[] dk = { "Fabel", "Fame Trend", "Farfalla", "Fashion Silk",
					"Filolino", "Fino", "Flusi", "Fortissima Colori",
					"Fortissima Socka Bambou", "Freedom Gorgeous DK",
					"Freedom Spirit", "Fresco", "Fritidsgarn", "Fundus",
					"Funny", "Funny Glitter", "Galaxie Saturn", "Geologie",
					"Glitter", "Gnocchi", "Grande", "Granite" };

			String[] worsted = { "Hand Paint Chunky", "Hand Paint Sock Yarn",
					"Hand Paint Worsted", "Hand-Dye Effect", "Handspun",
					"Highlander", "Ice", "Iceland", "In-Silk", "Jasper",
					"Jawoll Color Aktion", "Jawoll Silk",
					"Kid Mohair Hand Painted", "Kid-Seta", "Kid-Seta Lux",
					"Kid-Silk", "kidsilk haze", "Kitten Mohair",
					"Lanett Superwash", "Lava Hand Painted", "Lett-Lopi",
					"Life Aran", "Loft Classic", "Loft Color", "Lovely Jeans",
					"Loyal", "Lush", "Lustra", "Maggi�s Angora",
					"Maggi�s Cotton", "Maggi�s Irish Tweeds", "Maggi�s Linen",
					"Maggi�s Lurex", "Maggi�s Mist Slub", "Maggi�s Mohair",
					"Maggi�s Rag", "Maggi�s Tweed Fleck Aran",
					"Mandarin Petit", "Manifesto", "Marble", "Marble Chunky",
					"Mega Boots Stretch", "Meilenweit 2 in 1",
					"Meilenweit Bosco", "Cascade 220 Heathers",
					"Cascade 220 Solids", "Meilenweit Cotton Fantasy",
					"Meilenweit Magico", "Meilenweit Marmi",
					"Meilenweit Multieffekt", "Meilenweit Party",
					"Meilenweit Primo", "Meilenweit Solo", "Meilenweit Stile",
					"Meilenweit Sunset", "Melody", "MeriCash Hand Painted",
					"Merino Extra Fine", "Merino Lace", "Merino Superlight",
					"MeriSock Hand Painted", "Merisoft Hand Painted",
					"Merisoft Solid", "Merisoft Space Dyed",
					"MeriTwist Hand Painted", "Mille Colori Big",
					"Mini Alpakka", "MinnowMerino", "Mirage DK", "Mist",
					"Mojito", "Mosco", "Muskat", "Muskat Soft",
					"Natural Focus Ecologie Cotton",
					"Natural Focus Ecologie Wool", "NaturLin",
					"New England Highland", "New England Shetland", "Nimbus",
					"Nimbus Multi", "Northern Chunky", "Northern Chunky (new)",
					"Northern Worsted ombre", "Northern Worsted solid",
					"Northern Worsted tweed", "On Your Toes 4 ply",
					"On Your Toes Bamboo", "On Your Toes DK", "Opus 1",
					"Origin", "Ovation", "Pastaza", "Peruvia",
					"Peruvia Colors", "Polaris", "Polaris", "Puddel", "Punch",
					"Pure Luxury Merino", "Pure Pima",
					"Purelife Organic Cotton", "Quick", "Rejuvenation",
					"Riana Big Color", "Rimu", "Riverstone Mohair", "Safran",
					"Sapphire D.K.", "Sarek", "Scherzo", "Secondo", "Seduce",
					"Sierra", "Silk Color", "Silke-Alpaca", "Silke-Tweed",
					"Silky Alpaca Lace", "Sisu", "SISU Fantasy", "Smart",
					"Socka Bamboo", "Sockina Cotton", "Socrates",
					"Soft Chunky", "Soft Chunky", "Softy", "Softy Color",
					"Sojabama", "Solstice", "South American", "Sox",
					"Sox Metallic", "Spiaggia", "Splash Baby DK", "Stella",
					"Strompegarn", "Summer Sox", "Super Soxx Cotton",
					"Supercotton", "Supercotton Color", "Superlana",
					"Symphony", "Temptation", "Tiffany", "Tiramisu",
					"Tonalita", "Tove", "Trekking 6ply", "Trekking hand art",
					"Trekking pro natura", "Trekking XXL", "Trenton Worsted",
					"Trilogy", "Tweed Montage", "twelve", "Ultra Alpaca",
					"Ultra Alpaca Fine", "Unikat" };

			String[] aran = { "Urban Silk", "Vero", "Vero Tweed", "Vintage",
					"Vivaldi", "Wild Willow" };

			String[] bulky = { "Willow", "Wondersoft D.K.", "Woodland",
					"Woolly Bully" };

			String[] superBulky = { "Zara", "Zarina Baby Print" };

			mYarnAssociations.put(weights[0],
					new ArrayList<String>(Arrays.asList(none)));
			mYarnAssociations.put(weights[1],
					new ArrayList<String>(Arrays.asList(thread)));
			mYarnAssociations.put(weights[2],
					new ArrayList<String>(Arrays.asList(cobweb)));
			mYarnAssociations.put(weights[3],
					new ArrayList<String>(Arrays.asList(lace)));
			mYarnAssociations.put(weights[4],
					new ArrayList<String>(Arrays.asList(lightFingering)));
			mYarnAssociations.put(weights[5],
					new ArrayList<String>(Arrays.asList(fingering)));
			mYarnAssociations.put(weights[6],
					new ArrayList<String>(Arrays.asList(sport)));
			mYarnAssociations.put(weights[7],
					new ArrayList<String>(Arrays.asList(dk)));
			mYarnAssociations.put(weights[8],
					new ArrayList<String>(Arrays.asList(worsted)));
			mYarnAssociations.put(weights[9],
					new ArrayList<String>(Arrays.asList(aran)));
			mYarnAssociations.put(weights[10],
					new ArrayList<String>(Arrays.asList(bulky)));
			mYarnAssociations.put(weights[11],
					new ArrayList<String>(Arrays.asList(superBulky)));
		}
	}
}
