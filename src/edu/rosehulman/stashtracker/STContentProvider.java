package edu.rosehulman.stashtracker;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

public class STContentProvider extends ContentProvider {

	public static final Uri CONTENT_URI = Uri
			.parse("content://edu.rosehulman.stashtracker/elements");

	private static final int ALLROWS = 1;
	private static final int SINGLE_ROW = 2;

	private static final UriMatcher uriMatcher;

	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI("edu.rosehulman.stashtracker", "elements", ALLROWS);
		uriMatcher.addURI("edu.rosehulman.stashtracker", "elements/#",
				SINGLE_ROW);
	}

	public static final String KEY_ID = "_id";

	public static final String KEY_YARN_NAME_COLUMN = "KEY_YARN_NAME_COLUMN";
	public static final String KEY_YARN_WEIGHT_COLUMN = "KEY_YARN_WEIGHT_COLUMN";
	public static final String KEY_YARN_AMT_STASHED_COLUMN = "KEY_YARN_AMT_STASHED_COLUMN";
	// TODO: Create public field for each column in your table

	private STDBOpenHelper myOpenHelper;

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		SQLiteDatabase db = myOpenHelper.getWritableDatabase();

		switch (uriMatcher.match(uri)) {
		case SINGLE_ROW:
			String rowID = uri.getPathSegments().get(1);
			selection = KEY_ID
					+ "="
					+ rowID
					+ (!TextUtils.isEmpty(selection) ? " AND (" + selection
							+ ')' : " ");
		default:
			break;
		}

		if (selection == null) {
			selection = "1";
		}
		int deleteCount = db.delete(STDBOpenHelper.DATABASE_TABLE, selection,
				selectionArgs);
		getContext().getContentResolver().notifyChange(uri, null);

		return deleteCount;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		SQLiteDatabase db = myOpenHelper.getWritableDatabase();

		String nullColumnHack = null;

		long id = db.insert(STDBOpenHelper.DATABASE_TABLE, nullColumnHack,
				values);

		if (id > -1) {
			Uri intertedId = ContentUris.withAppendedId(CONTENT_URI, id);

			getContext().getContentResolver().notifyChange(intertedId, null);
			return intertedId;
		} else {
			return null;
		}
	}

	@Override
	public boolean onCreate() {
		myOpenHelper = new STDBOpenHelper(getContext(),
				STDBOpenHelper.DATABASE_NAME, null,
				STDBOpenHelper.DATABASE_VERSION);
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		SQLiteDatabase db = myOpenHelper.getWritableDatabase();

		String groupBy = null;
		String having = null;

		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		queryBuilder.setTables(STDBOpenHelper.DATABASE_TABLE);

		switch (uriMatcher.match(uri)) {
		case SINGLE_ROW:
			String rowID = uri.getPathSegments().get(1);
			queryBuilder.appendWhere(KEY_ID + "=" + rowID);
		default:
			break;
		}

		Cursor cursor = queryBuilder.query(db, projection, selection,
				selectionArgs, groupBy, having, sortOrder);

		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		SQLiteDatabase db = myOpenHelper.getWritableDatabase();

		switch (uriMatcher.match(uri)) {
		case SINGLE_ROW:
			String rowID = uri.getPathSegments().get(1);
			selection = KEY_ID
					+ "="
					+ rowID
					+ (!TextUtils.isEmpty(selection) ? " AND (" + selection
							+ ')' : " ");
		default:
			break;
		}

		int updateCount = db.update(STDBOpenHelper.DATABASE_TABLE, values,
				selection, selectionArgs);
		getContext().getContentResolver().notifyChange(uri, null);

		return updateCount;

	}

}
