package edu.rosehulman.stashtracker;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Yarn implements Comparable<Yarn> {

	private int mID;
	private String mName;
	private int mWeight;
	private String mAmtStashed;
	private String mPurchasedAt;
	private GregorianCalendar mPurchaseDate;
	private String mPricePaid;
	private String mDyeLot;
	private String mColorWay;
	private int mColorFamily;
	private String mTags;
	private String mNotes;

	public int compareTo(Yarn another) {
		return 0;
	}

	@Override
	public String toString() {
		String s = "";
		s += "name: " + mName;
		s += "\n";
		s += "weight: " + mWeight;
		s += "\n";
		s += "amount: " + mAmtStashed;
		s += "\n";
		s += "purchased at: " + mPurchasedAt;
		s += "\n";
		s += "purchase date: " + getPurchaseDateAsString();
		s += "\n";
		s += "price: " + mPricePaid;
		s += "\n";
		s += "dye lot: " + mDyeLot;
		s += "\n";
		s += "color way: " + mColorWay;
		s += "\n";
		s += "color family: " + mColorFamily;
		s += "\n";
		s += "tags: " + mTags;
		s += "\n";
		s += "notes: " + mNotes;
		s += "\n";

		return s;
	}

	public int getID() {
		return mID;
	}

	public void setID(int id) {
		mID = id;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		mName = name;
	}

	public int getWeight() {
		return mWeight;
	}

	public String getWeightAsString() {
		String[] weights = MainActivity.res
				.getStringArray(R.array.yarn_weights);
		return weights[mWeight];
	}

	public void setWeight(int weight) {
		mWeight = weight;
	}

	public String getAmtStashed() {
		return mAmtStashed;
	}

	public void setAmtStashed(String amtStashed) {
		mAmtStashed = amtStashed;
	}

	public String getPurchasedAt() {
		return mPurchasedAt;
	}

	public void setPurchasedAt(String purchasedAt) {
		mPurchasedAt = purchasedAt;
	}

	public GregorianCalendar getPurchaseDate() {
		return mPurchaseDate;
	}

	public String getPurchaseDateAsString() {
		return mPurchaseDate.get(Calendar.MONTH) + "/"
				+ mPurchaseDate.get(Calendar.DAY_OF_MONTH) + "/"
				+ mPurchaseDate.get(Calendar.YEAR);
	}

	public void setPurchaseDate(GregorianCalendar purchaseDate) {
		mPurchaseDate = purchaseDate;
	}

	public void setPurchaseDate(int year, int month, int day) {
		mPurchaseDate = new GregorianCalendar(year, month, day);
	}

	public String getPricePaid() {
		return mPricePaid;
	}

	public void setPricePaid(String pricePaid) {
		mPricePaid = pricePaid;
	}

	public String getDyeLot() {
		return mDyeLot;
	}

	public void setDyeLot(String dyeLot) {
		mDyeLot = dyeLot;
	}

	public String getColorWay() {
		return mColorWay;
	}

	public void setColorWay(String colorWay) {
		mColorWay = colorWay;
	}

	public int getColorFamily() {
		return mColorFamily;
	}

	public String getColorFamilyAsString() {
		String[] colors = MainActivity.res
				.getStringArray(R.array.closest_color);
		return colors[mColorFamily];
	}

	public void setColorFamily(int colorFamily) {
		mColorFamily = colorFamily;
	}

	public String getTags() {
		return mTags;
	}

	public void setTags(String tags) {
		mTags = tags;
	}

	public String getNotes() {
		return mNotes;
	}

	public void setNotes(String notes) {
		mNotes = notes;
	}

}
