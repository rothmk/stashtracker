package edu.rosehulman.stashtracker;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class STAdapter {

	public static final String DATABASE_NAME = "StashTrackerDB.db";
	public static final String DATABASE_TABLE = "YarnInfo";
	// TODO if database changes, increment DATABASE_VERSION
	public static final int DATABASE_VERSION = 14;

	private SQLiteOpenHelper myOpenHelper;
	private SQLiteDatabase mDb;

	public static final String KEY_ID = "_id";
	public static final String KEY_YARN_NAME_COLUMN = "name";
	public static final String KEY_YARN_WEIGHT_COLUMN = "weight";
	public static final String KEY_YARN_AMT_STASHED_COLUMN = "amt_stashed";
	public static final String KEY_YARN_PURCHASED_AT_COLUMN = "purchased_at";
	public static final String KEY_YARN_PURCHASE_DATE_COLUMN = "purchase_date";
	public static final String KEY_YARN_PRICE_PAID_COLUMN = "price_paid";
	public static final String KEY_YARN_DYE_LOT_COLUMN = "dye_lot";
	public static final String KEY_YARN_COLORWAY_COLUMN = "colorway";
	public static final String KEY_YARN_COLOR_FAMILY_COLUMN = "color_family";
	public static final String KEY_YARN_TAGS_COLUMN = "tags";
	public static final String KEY_YARN_NOTES_COLUMN = "notes";

	private static String DATABASE_DROP = "DROP TABLE IF EXISTS "
			+ DATABASE_TABLE;
	private static final String DATABASE_CREATE = "create table "
			+ DATABASE_TABLE + " (" + KEY_ID
			+ " integer primary key autoincrement, " + KEY_YARN_NAME_COLUMN
			+ " text not null, " + KEY_YARN_WEIGHT_COLUMN + " integer, "
			+ KEY_YARN_AMT_STASHED_COLUMN + " integer, "
			+ KEY_YARN_PURCHASED_AT_COLUMN + " text, "
			+ KEY_YARN_PURCHASE_DATE_COLUMN + " integer, "
			+ KEY_YARN_PRICE_PAID_COLUMN + " float, " + KEY_YARN_DYE_LOT_COLUMN
			+ " integer, " + KEY_YARN_COLORWAY_COLUMN + " text, "
			+ KEY_YARN_COLOR_FAMILY_COLUMN + " integer, "
			+ KEY_YARN_TAGS_COLUMN + " text, " + KEY_YARN_NOTES_COLUMN
			+ " text" + ");";

	public STAdapter(Context context) {
		myOpenHelper = new STDbHelper(context);
	}

	public void open() {
		mDb = myOpenHelper.getWritableDatabase();
	}

	public void close() {
		mDb.close();
	}

	public long addYarn(Yarn yarn) {
		ContentValues rowValues = getContentValuesFromYarn(yarn);
		return mDb.insert(DATABASE_TABLE, null, rowValues);
	}

	public List<Yarn> getAllYarn() {
		List<Yarn> allYarn = new ArrayList<Yarn>();

		Cursor cursor = mDb.query(DATABASE_TABLE, new String[] { KEY_ID,
				KEY_YARN_NAME_COLUMN, KEY_YARN_WEIGHT_COLUMN,
				KEY_YARN_AMT_STASHED_COLUMN, KEY_YARN_PURCHASED_AT_COLUMN,
				KEY_YARN_PURCHASE_DATE_COLUMN, KEY_YARN_PRICE_PAID_COLUMN,
				KEY_YARN_DYE_LOT_COLUMN, KEY_YARN_COLORWAY_COLUMN,
				KEY_YARN_COLOR_FAMILY_COLUMN, KEY_YARN_TAGS_COLUMN,
				KEY_YARN_NOTES_COLUMN }, null, null, null, null, KEY_ID
				+ " DESC");

		if (cursor.moveToFirst()) {
			allYarn.add(getYarnFromCursor(cursor));
			Log.d(MainActivity.ST, "got yarn: " + getYarnFromCursor(cursor));
		}

		while (cursor.moveToNext()) {
			Log.d(MainActivity.ST, "got yarn: " + getYarnFromCursor(cursor));
			allYarn.add(getYarnFromCursor(cursor));
		}

		return allYarn;
	}

	public List<String> getAllYarnNames() {
		List<String> allNames = new ArrayList<String>();

		Cursor cursor = mDb.query(DATABASE_TABLE, new String[] { KEY_ID,
				KEY_YARN_NAME_COLUMN, KEY_YARN_WEIGHT_COLUMN,
				KEY_YARN_AMT_STASHED_COLUMN, KEY_YARN_PURCHASED_AT_COLUMN,
				KEY_YARN_PURCHASE_DATE_COLUMN, KEY_YARN_PRICE_PAID_COLUMN,
				KEY_YARN_DYE_LOT_COLUMN, KEY_YARN_COLORWAY_COLUMN,
				KEY_YARN_COLOR_FAMILY_COLUMN, KEY_YARN_TAGS_COLUMN,
				KEY_YARN_NOTES_COLUMN }, null, null, null, null, KEY_ID
				+ " DESC");

		if (cursor.moveToFirst()) {
			allNames.add(getYarnFromCursor(cursor).getName());
			Log.d(MainActivity.ST, "got yarn name: "
					+ getYarnFromCursor(cursor).getName());
		}

		while (cursor.moveToNext()) {
			Log.d(MainActivity.ST, "got yarn name: "
					+ getYarnFromCursor(cursor).getName());
			allNames.add(getYarnFromCursor(cursor).getName());
		}

		return allNames;
	}

	public Yarn getYarn(long id) {
		//trying to query the database kept telling me that the column for the input name didn't exist
				//that wasn't making any sense so I made it work like this
		
		// Getting a cursor for the specific id
//		String[] projection = new String[] { KEY_ID, KEY_YARN_NAME_COLUMN,
//				KEY_YARN_WEIGHT_COLUMN, KEY_YARN_AMT_STASHED_COLUMN,
//				KEY_YARN_PURCHASED_AT_COLUMN, KEY_YARN_PURCHASE_DATE_COLUMN,
//				KEY_YARN_PRICE_PAID_COLUMN, KEY_YARN_DYE_LOT_COLUMN,
//				KEY_YARN_COLORWAY_COLUMN, KEY_YARN_COLOR_FAMILY_COLUMN,
//				KEY_YARN_TAGS_COLUMN, KEY_YARN_NOTES_COLUMN };
//		String selection = KEY_ID + " =" + id;
//		Cursor c = mDb.query(true, DATABASE_TABLE, projection, selection, null,
//				null, null, null, "1");
//
//		// Using the cursor to get values from the table
//		if (c != null && c.moveToFirst()) {
//			getYarnFromCursor(c);
//		}
//		return null;
		
		Yarn yarn = new Yarn();
		List<Yarn> allYarn = getAllYarn();
		for(int i = 0; i < allYarn.size(); i++){
			yarn = allYarn.get(i); 
			if(yarn.getID() == id){
				return yarn; 
			}
		}
		return yarn;
	}

	public Yarn getYarn(String name) {
		//trying to query the database kept telling me that the column for the input name didn't exist
		//that wasn't making any sense so I made it work like this 
		
		// Getting a cursor for the specific id
//		String[] columns = new String[] { KEY_ID, KEY_YARN_NAME_COLUMN,
//				KEY_YARN_WEIGHT_COLUMN, KEY_YARN_AMT_STASHED_COLUMN,
//				KEY_YARN_PURCHASED_AT_COLUMN, KEY_YARN_PURCHASE_DATE_COLUMN,
//				KEY_YARN_PRICE_PAID_COLUMN, KEY_YARN_DYE_LOT_COLUMN,
//				KEY_YARN_COLORWAY_COLUMN, KEY_YARN_COLOR_FAMILY_COLUMN,
//				KEY_YARN_TAGS_COLUMN, KEY_YARN_NOTES_COLUMN };
//		String selection = KEY_YARN_NAME_COLUMN + " = " + name;
//		Cursor c = mDb.query(true, DATABASE_TABLE, columns, selection,
//				null, null, null, null, "1");
//
//		// Using the cursor to get values from the table
//		if (c != null && c.moveToFirst()) {
//			getYarnFromCursor(c);
//		}
//		if(c == null){
//			Log.d(MainActivity.ST, "nothing returned :(");
//		}
//		return null;
		Yarn yarn = new Yarn();
		List<Yarn> allYarn = getAllYarn();
		for(int i = 0; i < allYarn.size(); i++){
			yarn = allYarn.get(i); 
			if(yarn.getName().equals(name)){
				return yarn; 
			}
		}
		return yarn;
	}

	public Yarn getYarnFromCursor(Cursor c) {
		Yarn yarn = new Yarn();

		int idColumn = c.getColumnIndexOrThrow(KEY_ID);
		int nameColumn = c.getColumnIndexOrThrow(KEY_YARN_NAME_COLUMN);
		int weightColumn = c.getColumnIndexOrThrow(KEY_YARN_WEIGHT_COLUMN);
		int amtColumn = c.getColumnIndexOrThrow(KEY_YARN_AMT_STASHED_COLUMN);
		int purchasedAt = c.getColumnIndexOrThrow(KEY_YARN_PURCHASED_AT_COLUMN);
		int purchaseDate = c
				.getColumnIndexOrThrow(KEY_YARN_PURCHASE_DATE_COLUMN);
		int price = c.getColumnIndexOrThrow(KEY_YARN_PRICE_PAID_COLUMN);
		int dyeLot = c.getColumnIndexOrThrow(KEY_YARN_DYE_LOT_COLUMN);
		int colorWay = c.getColumnIndexOrThrow(KEY_YARN_COLORWAY_COLUMN);
		int colorFamily = c.getColumnIndexOrThrow(KEY_YARN_COLOR_FAMILY_COLUMN);
		int tags = c.getColumnIndexOrThrow(KEY_YARN_TAGS_COLUMN);
		int notes = c.getColumnIndexOrThrow(KEY_YARN_NOTES_COLUMN);

		yarn.setID(c.getInt(idColumn));
		yarn.setName(c.getString(nameColumn));
		yarn.setWeight(c.getInt(weightColumn));
		yarn.setAmtStashed(c.getString(amtColumn));
		yarn.setPurchasedAt(c.getString(purchasedAt));

		GregorianCalendar date = new GregorianCalendar();
		date.setTimeInMillis(c.getLong(purchaseDate));
		yarn.setPurchaseDate(date);

		yarn.setPricePaid(c.getString(price));
		yarn.setDyeLot(c.getString(dyeLot));
		yarn.setColorWay(c.getString(colorWay));
		yarn.setColorFamily(c.getInt(colorFamily));
		yarn.setTags(c.getString(tags));
		yarn.setNotes(c.getString(notes));

		return yarn;
	}

	public void updateYarn(Yarn yarn) {
		ContentValues rowValues = getContentValuesFromYarn(yarn);

		String whereClause = KEY_ID + "=" + yarn.getID();
		mDb.update(DATABASE_TABLE, rowValues, whereClause, null);
	}

	public boolean removeYarn(long id) {
		return mDb.delete(DATABASE_TABLE, KEY_ID + "=" + id, null) > 0;
	}

	public boolean removeYarn(Yarn yarn) {
		return removeYarn(yarn.getID());
	}

	private ContentValues getContentValuesFromYarn(Yarn yarn) {
		ContentValues rowValues = new ContentValues();

		rowValues.put(KEY_YARN_NAME_COLUMN, yarn.getName());
		rowValues.put(KEY_YARN_WEIGHT_COLUMN, yarn.getWeight());
		rowValues.put(KEY_YARN_AMT_STASHED_COLUMN, yarn.getAmtStashed());
		rowValues.put(KEY_YARN_PURCHASED_AT_COLUMN, yarn.getPurchasedAt());

		if (yarn.getPurchaseDate() != null) {
			rowValues.put(KEY_YARN_PURCHASE_DATE_COLUMN, yarn.getPurchaseDate()
					.getTimeInMillis());
		}
		rowValues.put(KEY_YARN_PRICE_PAID_COLUMN, yarn.getPricePaid());
		rowValues.put(KEY_YARN_DYE_LOT_COLUMN, yarn.getDyeLot());
		rowValues.put(KEY_YARN_COLORWAY_COLUMN, yarn.getColorWay());
		rowValues.put(KEY_YARN_COLOR_FAMILY_COLUMN, yarn.getColorFamily());
		rowValues.put(KEY_YARN_TAGS_COLUMN, yarn.getTags());
		rowValues.put(KEY_YARN_NOTES_COLUMN, yarn.getNotes());

		return rowValues;
	}

	public Cursor getYarnCursor() {
		String[] projection = new String[] { KEY_ID, KEY_YARN_NAME_COLUMN,
				KEY_YARN_WEIGHT_COLUMN, KEY_YARN_AMT_STASHED_COLUMN,
				KEY_YARN_PURCHASED_AT_COLUMN, KEY_YARN_PURCHASE_DATE_COLUMN,
				KEY_YARN_PRICE_PAID_COLUMN, KEY_YARN_DYE_LOT_COLUMN,
				KEY_YARN_COLORWAY_COLUMN, KEY_YARN_COLOR_FAMILY_COLUMN,
				KEY_YARN_TAGS_COLUMN, KEY_YARN_NOTES_COLUMN };
		return mDb.query(DATABASE_TABLE, projection, null, null, null, null,
				KEY_YARN_NAME_COLUMN + " DESC");
	}

	public boolean clearDb() {

		Cursor cursor = mDb.query(DATABASE_TABLE, new String[] { KEY_ID,
				KEY_YARN_NAME_COLUMN, KEY_YARN_WEIGHT_COLUMN,
				KEY_YARN_AMT_STASHED_COLUMN, KEY_YARN_PURCHASED_AT_COLUMN,
				KEY_YARN_PURCHASE_DATE_COLUMN, KEY_YARN_PRICE_PAID_COLUMN,
				KEY_YARN_DYE_LOT_COLUMN, KEY_YARN_COLORWAY_COLUMN,
				KEY_YARN_COLOR_FAMILY_COLUMN, KEY_YARN_TAGS_COLUMN,
				KEY_YARN_NOTES_COLUMN }, null, null, null, null, KEY_ID
				+ " DESC");

		if (cursor.moveToFirst()) {
			Log.d(MainActivity.ST, "removing yarn: "
					+ getYarnFromCursor(cursor));
			removeYarn(getYarnFromCursor(cursor));
		}

		while (cursor.moveToNext()) {
			Log.d(MainActivity.ST, "removing yarn: "
					+ getYarnFromCursor(cursor));
			removeYarn(getYarnFromCursor(cursor));
		}

		return true;
	}

	private static class STDbHelper extends SQLiteOpenHelper {

		public STDbHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.d(MainActivity.ST, "Updating from version " + oldVersion
					+ " to " + newVersion
					+ ", which will destroy old table(s).");
			db.execSQL(DATABASE_DROP);
			onCreate(db);
		}

	}
}
