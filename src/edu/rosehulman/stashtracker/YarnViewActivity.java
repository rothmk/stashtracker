package edu.rosehulman.stashtracker;

import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class YarnViewActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_yarn_view);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		ListView listView = (ListView) findViewById(R.id.yarn_listview);
		final List<String> mYarnList = MainActivity.mDbSTAdapter
				.getAllYarnNames();
		Collections.sort(mYarnList);

		ArrayAdapter<String> mYarnAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, mYarnList);
		listView.setAdapter(mYarnAdapter);

		// final String[] yarnArray = new String[]{}; /*{ "Sabine", "Findley",
		// "Red Heart", "Malabrigo", "Findley Dappled",
		// "Holiday Sock Yarn", "Cascade 220", "CoBaSi",
		// "Crazy Zauberball", "Green Sock Yarn" };*/
		// while(MainActivity.mSTCursor.moveToNext()){
		// int i = 0;
		// yarnArray[i] =
		// MainActivity.mDbSTAdapter.getYarnFromCursor(MainActivity.mSTCursor).getName();
		// i++;
		// }

		// First paramenter - Context
		// Second parameter - Layout for the row
		// Third parameter - ID of the TextView to which the data is written
		// Forth - the Array of data
		// ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
		// android.R.layout.simple_list_item_1, android.R.id.text1, yarnArray);

		listView.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				Intent viewYarnDetailsIntent = new Intent(YarnViewActivity.this,
						YarnDetailViewActivity.class);
				viewYarnDetailsIntent.putExtra(MainActivity.KEY_NEW_YARN, false);
				viewYarnDetailsIntent.putExtra(MainActivity.KEY_YARN_NAME,
						mYarnList.get(position));
				YarnViewActivity.this.startActivity(viewYarnDetailsIntent);

			}
		});
		
		listView.setOnItemLongClickListener(new OnItemLongClickListener() {

			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				Yarn yarn = MainActivity.getYarn(mYarnList.get(position));
				MainActivity.removeYarn(yarn);
				recreate();
				return false;
			}
		});

		// Assign adapter to ListView
		listView.setAdapter(mYarnAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_yarn_view, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent homeIntent = new Intent(YarnViewActivity.this,
					MainActivity.class);
			YarnViewActivity.this.startActivity(homeIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
